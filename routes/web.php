<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvoiceMail;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('email');
});

Route::get('/send', function () {
    $mail = new InvoiceMail();

    $list = [
        'mark@acero.industries',
        'sharon@allsteelcarports.com',
        'fabian@grupocomunicado.com',
        'michael@allsteelcarports.com',
        'alan@allamericanbuildings.com'
    ];
    $ml = Mail::to('fabian@grupocomunicado.com');
    //$ml->cc($list);
    $ml->bcc('victor@grupocomunicado.com');
    //$ml = Mail::to('victor@grupocomunicado.com');
    //$ml->send($mail);
    dd('send');
});
